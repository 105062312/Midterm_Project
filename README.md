# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* [Talking Box]
* Key functions 
    1. Sign in & sign up
    2. chat
    3. load message history
    4. chat with new user
* Other functions (add/delete)
    1. 顯示訊息時間
    2. 可上傳圖片訊息
    3. 有新訊息實顯示通知，點擊通知即可開啟聊天頁面，或未登入輸入訊息會顯示提醒
    4. RWD
    5. CSS animation (左上角標題文字跳動)
    6. 若使用google登入能顯示使用者頭像

## Basic Components
|Component|Score|Y|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|N|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|
|Other functions|1~10%|Y|

## Website Detail Description
作品網址: https://ss-midterm-project-e7d53.firebaseapp.com/

報告網址: https://gitlab.com/105062312/Midterm_Project/blob/master/README.md

參考的template: https://github.com/firebase/friendlychat-web 以及助教提供的lab 6 code

* Description
    js檔案中code的有註解，在此就只為幾個主要功能做介紹。
    1. 信箱登入、google登入: 使用助教提供的template實作，在畫面右上方點擊 Sign in with，選擇google，即可使用google帳號登入;
    或是在 e-mail 和 password欄位中填寫正確信箱格式，密碼需長於六個字元，在 Sign in with 選擇Sign Up以註冊，
    或是已註冊過的使用者填寫完信箱密碼後，在Sign in with 選擇E-mail以登入。
    若輸入格式錯誤會跳出提醒。
    code部分參見main.js line 242
    2. 傳訊息、圖片聊天: 點擊下方"Message..."欄位，輸入訊息，然後按enter或是滑鼠點擊旁邊的send按鈕以送出，
    或是點擊欄位右側的圖片圖示選擇圖片並上傳。
    若沒有登入則不能使用聊天功能，會跳出提醒使用者需登入以繼續。
    顯示的訊息內容包含:使用者ID、頭像、訊息內容以及傳送時間。 
    code參見main.js line 77 開始的一系列code
    3. google通知: 只要聊天室有新訊息，右下角就會顯示通知，點擊可開啟聊天頁面。
    code參見main.js line 93
    4. css animation: 網路上找的小功能，引入HTML之後使用即可。效果為左上角小標題彈跳。
    code參見index.html line 59 和 68
    
    以上為主要功能講解。
    
## Security Report (Optional)
1. 輸入訊息非使用innerHTML，以避免使用者輸入HTML格式訊息時造成版面毀損，甚至藉此寫入函式竊取資料
2.  Storage Security Rules的設置預設為所有登入的成員都能寫入或讀取資料庫的內容，而我修改成只能自己只能寫入自己的資料夾。
    Database Security Rules的設置預設為所有登入的成員都能寫入或讀取firebase及時資料庫的內容，而我修改成只能讀/寫自己的Firebase Cloud Messaging device tokens。